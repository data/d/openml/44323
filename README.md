# OpenML dataset: Meta_Album_CRS_Extended

https://www.openml.org/d/44323

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Cars Dataset (Extended)**
***
The original Cars dataset (https://ai.stanford.edu/~jkrause/cars/car_dataset.html) was collected in 2013, and it contains more than 16 000 images from 196 classes of cars. Most images are on the road, but some have different backgrounds, and each image has only one car. Each class can have 48 to 136 images of variable resolutions. The preprocess version for this dataset was obtained by creating square images either duplicating the top and bottom-most 3 rows or the left and right most 3 columns based on the orientation of the original image. In this case, cropping was not applied to create the square images since following this technique results in losing too much information from the cars. Then, the square images were resized into 128x128 px using an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/CRS.png)

**Meta Album ID**: VCL.CRS  
**Meta Album URL**: [https://meta-album.github.io/datasets/CRS.html](https://meta-album.github.io/datasets/CRS.html)  
**Domain ID**: VCL  
**Domain Name**: Vehicles  
**Dataset ID**: CRS  
**Dataset Name**: Cars  
**Short Description**: Dataset with images of different car models  
**\# Classes**: 196  
**\# Images**: 16185  
**Keywords**: vehicles, cars  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: ImageNet License  
**License URL(original data release)**: https://ai.stanford.edu/~jkrause/cars/car_dataset.html
https://www.image-net.org/download.php
 
**License (Meta-Album data release)**: ImageNet License  
**License URL (Meta-Album data release)**: [https://www.image-net.org/download.php](https://www.image-net.org/download.php)  

**Source**: Stanford Cars Dataset  
**Source URL**: https://ai.stanford.edu/~jkrause/cars/car_dataset.html  
  
**Original Author**: Jonathan Krause, Michael Stark, Jia Deng, Li Fei-Fei  
**Original contact**: jkrause@cs.stanford.edu  

**Meta Album author**: Philip Boser  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{KrauseStarkDengFei-Fei_3DRR2013,
  title = {3D Object Representations for Fine-Grained Categorization},
  booktitle = {4th International IEEE Workshop on  3D Representation and Recognition (3dRR-13)},
  year = {2013},
  address = {Sydney, Australia},
  author = {Jonathan Krause and Michael Stark and Jia Deng and Li Fei-Fei}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44245)  [[Mini]](https://www.openml.org/d/44289)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44323) of an [OpenML dataset](https://www.openml.org/d/44323). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44323/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44323/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44323/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

